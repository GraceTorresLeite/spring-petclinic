package org.springframework.samples.petclinic.owner;

import java.util.Collection;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller

public class PetTypeController {

	private static final String VIEWS_PETTYPE_CREATE_OR_UPDATE_FORM = "pets/createOrUpdatePettypeForm";

	private final PetTypeRepository types;
	

	public PetTypeController(PetTypeRepository types) {
		this.types = types;
	}

	@ModelAttribute("types")
	public Collection<PetType> populatePetTypes() {
		return this.types.findPetTypes();
	}

	@GetMapping("/pettypes")
	public String showPetTypeList(Model model) {
		// TODO: consultar banco de dados!
		// FIXME: Mock up list!!

//		pets = new ArrayList<PetType>();
//
//		String[] petNames = { "cat", "dog", "lizard", "snake", "bird", "hamster" };
//		for (String n : petNames) {
//			PetType p = new PetType();
//			p.setName(n);
//			pets.add(p);
//		}
		Collection<PetType> lista = this.types.findPetTypes();
		model.addAttribute("types", lista);

		return "pets/petTypeList";
	}

	@GetMapping("/pettypes/new")
	public String initCreationForm(Map<String, Object> model) {
		
		PetType petType = new PetType();
		model.put("petType", petType);
		String[] categories = { "exotic", "domestic" };
		model.put("categories", categories);

		return VIEWS_PETTYPE_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/pettypes/new") // BindingResult result, não consegui utilizar sem erros
	public String processCreationForm(PetType petType) {
		this.types.save(petType);
		
		return "redirect:/pettypes/" + petType.getId();
	}

	@GetMapping("/pettypes/{pettypeId}/edit")
	public String initUpdateOwnerForm(@PathVariable("pettypeId") int pettypeId, Model model) {
		
		PetType petType = this.types.findById(pettypeId);
		model.addAttribute("petType", petType);
		String[] categories = { "exotic", "domestic" };
		model.addAttribute("categories", categories);
		
		return VIEWS_PETTYPE_CREATE_OR_UPDATE_FORM;
	}

	@PostMapping("/pettypes/{pettypeId}/edit") // BindingResult result, não consegui utilizar sem erros
	public String processUpdatePetTypeForm(PetType ptype, @PathVariable("pettypeId") int pettypeId, ModelMap model) {

		ptype.setId(pettypeId);
		this.types.save(ptype);
		model.put("types", types);

		return "redirect:/pettypes/{pettypeId}";
	}

	// exemplos sem o banco

//	@GetMapping("/pettypes/{pettypeId}")
//	public String showPetType(@PathVariable("pettypeId") 
//	int pettypeId, Map<String, Object> model) {
//
////				PetType type = pets.stream()
////						.filter(pettype -> pettypeName.equals(pettype.getName()))
////						.findAny()
////						.orElse(null);
//				
//		model.put("pettype", this.types.findById(pettypeId));
//		return "pets/petTypeDetails";
//	}

	@GetMapping("/pettypes/{pettypeId}")
	public String showPetType(@PathVariable("pettypeId") int pettypeId, Map<String, Object> model) {

		model.put("pettype", this.types.findById(pettypeId));
		return "pets/petTypeDetails";
	}

}
