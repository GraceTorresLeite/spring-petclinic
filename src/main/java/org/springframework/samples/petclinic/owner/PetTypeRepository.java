package org.springframework.samples.petclinic.owner;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;


public interface PetTypeRepository extends Repository<PetType, Integer> {
	
	@Query("SELECT ptype FROM PetType ptype")
	List<PetType> findPetTypes();
	
	PetType findByName(String name);
	
	PetType findById(Integer id);
	
	void save (PetType type);
	
	void delete(PetType type);

}
