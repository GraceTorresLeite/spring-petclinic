package org.springframework.samples.petclinic.products;



public class Products {
	
	private String name;
	private Double price;

	public Products() {
		super();
	}
	
	
	public Products(String name, Double price) {
		super();
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
