package org.springframework.samples.petclinic.products;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProductsController {
		
		@GetMapping("/productsPet")
		public String showProductsPet(Model model) {

			
			Products[ ]listas = new Products[7]; 
			listas[0] = new Products ("Pet Food", 20.00);
			listas[1] = new Products ("Pet Collars", 15.00);
			listas[2] = new Products ("Pet Leashes", 20.00);
			listas[3] = new Products ("Pet Beds", 50.00);
			listas[4] = new Products ("Pet Toys", 10.00);
			listas[5] = new Products ("Pet Bowls", 30.00);
			listas[6] = new Products ("Pet Clothes", 35.00);
					
			model.addAttribute("name", listas);
			return "products/products";
			
		}
}
