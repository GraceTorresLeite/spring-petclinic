package org.springframework.samples.petclinic.vet;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;


public interface SpecialtyRepository extends Repository<Specialty, Integer> {
	
	@Query("SELECT specialty FROM Specialty specialty")
	List<Specialty> findSpecialty();
	
	Specialty findByName(String name);
	
	Specialty findById(Integer id);
	
	void save (Specialty type);
	
	void delete(Specialty type);

}
