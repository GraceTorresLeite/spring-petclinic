package org.springframework.samples.petclinic.vet;


import java.util.Collection;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class SpecialtyController {
	
	private final SpecialtyRepository specialty;
	
	
	public SpecialtyController(SpecialtyRepository specialty) {
		super();
		this.specialty = specialty;
	}

	@ModelAttribute("specialties")
	public Collection<Specialty> populateSpecialty() {
		return this.specialty.findSpecialty();
	}

	@GetMapping("/specialtiesVet")
	public String showEspecialtiesVet(Model model) {
		
		model.addAttribute("vets", populateSpecialty());
		
		return "vets/specialties";
		
	}
}
